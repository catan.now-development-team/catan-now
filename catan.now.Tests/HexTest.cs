﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using Xunit;
//using catan.now;
//
//namespace catan.now.test
//{
//    public class Hex_Test
//    {
//        private Hex hex;
//
//        public Hex_Test()
//        {
//            hex = new Hex(1,2);
//        }
//
//        [Fact]
//        public void TestAddScoresValue()
//        {
//            var list = new List<int>() { 1, 2, 3, 4, 5, 6 };
//
//            hex.Score.SetScore(true, true, list);
//            var result = hex.Score;
//
//            Assert.Equal(list[1], hex.Score.N);
//            Assert.Equal(list[4], hex.Score.Z);
//        }
//
//        [Fact]
//        public void TestAssignStarAlgorithm()
//        {
//            int[] numbers   = { 2, 3, 4, 5, 6, 8, 9, 10, 11, 12 };
//            int[] stars     = { 1, 2, 3, 4, 5, 0, 5, 4, 3, 2, 1 };
//
//            foreach(int n in numbers)
//            {
//                hex.RollPoint = n;
//                hex.AssignStar();
//                Assert.Equal(stars[n - 2], hex.Stars);
//            }
//        }
//    }
//}
