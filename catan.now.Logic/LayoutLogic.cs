﻿using catan.now.Logic.Interfaces;
using catan.now.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using catan.now.Model;
using Models;

namespace catan.now.Logic
{
    public class LayoutLogic : ILayoutLogic
    {
        private readonly ILayoutRepository _repository;

        public LayoutLogic(ILayoutRepository repository)
        {
            _repository = repository;
        }


        public Layout Create(Layout layout)
        {
            return _repository.Create(layout);
        }

        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }

        public Layout Read(int id)
        {
            return _repository.Read(id);
        }

        public Layout Update(Layout layout)
        {
            return _repository.Update(layout);
        }

        public Layout GetLayoutInfo(Guid id)
        {
            return _repository.GetLayoutInfo(id);
        }

        public void LinkHexesToLayout(List<Hex> hexes, int id)
        {
            _repository.LinkHexesToLayout(hexes, id);

        }

        public IEnumerable<Hex> GetHexesByGuid(Guid guid)
        {
            return _repository.GetHexesByGuid(guid);
        }
    }
}