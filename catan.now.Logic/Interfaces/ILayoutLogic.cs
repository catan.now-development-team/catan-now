﻿using System;
using System.Collections.Generic;
using System.Text;
using catan.now.Model;
using Models;

namespace catan.now.Logic.Interfaces
{
    public interface ILayoutLogic : ILogic<Layout>
    {

        Layout GetLayoutInfo(Guid id);
        
        void LinkHexesToLayout(List<Hex> hexes, int id);

        IEnumerable<Hex> GetHexesByGuid(Guid guid);
    }
}
