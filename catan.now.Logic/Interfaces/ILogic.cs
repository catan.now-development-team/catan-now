﻿using System;
using System.Collections.Generic;
using System.Text;

namespace catan.now.Logic.Interfaces
{
    public interface ILogic<T> where T : class
    {
        T Create(T entity);

        T Read(int id);

        T Update(T entity);

        bool Delete(int id);
    }
}
