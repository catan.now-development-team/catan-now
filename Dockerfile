FROM microsoft/dotnet:2.2-sdk

EXPOSE 80

COPY . catan.now/

WORKDIR catan.now

RUN dotnet publish -c Release catan.now

WORKDIR catan.now/bin/Release/netcoreapp2.2/publish

ENTRYPOINT ["dotnet", "catan.now.dll"]