﻿using System;
using System.Collections.Generic;
using System.Text;

namespace catan.now.Factory
{
    public class Factory
    {
        private Dictionary<Engine, string> _connectionStrings;

        public Factory(Dictionary<Engine, string> connectionStrings)
        {
            _connectionStrings = connectionStrings;
            foreach (var connectionString in _connectionStrings)
            {
                Console.WriteLine(connectionString.Key + "Value " + connectionString.Value);
            }
        }

        public IAccountLogic GetAccountLogic(Engine engine)
        {
            switch (engine)
            {
                case Engine.MySQL:
                    return new AccountLogic(new AccountRepository(new AccountMySqlContext(_connectionStrings[engine])));
                case Engine.Memory:
                    return new AccountLogic(new AccountRepository(new AccountMemoryContext()));
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
