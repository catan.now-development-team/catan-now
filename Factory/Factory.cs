﻿using catan.now.DAL.MySQL;
using catan.now.Logic;
using catan.now.Logic.Interfaces;
using catan.now.Model.Enums;
using catan.now.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace catan.now.Factory
{
    public class Factory
    {
        private readonly Dictionary<Engine, string> _connectionStrings;


        public Factory(Dictionary<Engine, string> connectionStrings)
        {
            _connectionStrings = connectionStrings;
            Console.WriteLine("Found " + connectionStrings.Count + " connection strings");
        }

        public ILayoutLogic GetLayoutLogic(Engine engine)
        {
            switch (engine)
            {
                case Engine.MySql:
                    return new LayoutLogic(new LayoutRepository(new LayoutMySqlContext(_connectionStrings[engine])));
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
