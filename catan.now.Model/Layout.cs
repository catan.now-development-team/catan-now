﻿using System;
using catan.now.Model.Utils;
using Models;

namespace catan.now.Model
{
    public class Layout : DbModel
    {
        [Property] public Guid Token { get; set; }

        public Board Board { get; set; }

        public Layout()
        {
            Board = new Board();
        }
    }
}