﻿using System.Collections;
using System.Collections.Generic;
using catan.now.Model;

namespace Models
{
    public class Board
    {

        public Board()
        {
            Hexes = new List<Hex>();
        }

        public int Id { get; set; }

        public List<Hex> Hexes { get; set; }
    }
}