namespace catan.now.Model.Enums
{
    public enum PortType
    {
        NotAPort,
        General, // 3 op 1 
        Wool,
        Wood,
        Ore,
        Brick,
        Grain
    }
}