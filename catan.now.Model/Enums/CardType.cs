namespace catan.now.Model.Enums
{
    public enum CardType
    {
        Water = 0,
        Port = 1,
        Wool = 2,
        Ore = 3,
        Wood = 4,
        Brick = 5,
        Grain = 6,
        Desert = 7,
        Empty = -1
    }
}