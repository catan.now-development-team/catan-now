﻿using catan.now.Model.Enums;
using catan.now.Model.Utils;
using Models;

namespace catan.now.Model
{
    public class Hex : DbModel
    {
        [Property] public int Id { get; set; }

        [Property] public int LayoutId { get; set; }

        [Property] public int CardType { get; set; }

        [Property] public int PortType { get; set; }

        [Property] public int X { get; set; }

        [Property] public int Y { get; set; }

        [Property] public int RollPoint { get; set; }

        [Property] public int Stars { get; set; }

        [Property] public int NorthScore { get; set; }

        [Property] public int SouthScore { get; set; }

        public bool NorthBool { get; set; }

        public bool SouthBool { get; set; }

        public Hex()
        {
        }

        public Hex(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Hex(int x, int y, CardType cardType)
        {
            X = x;
            Y = y;
            CardType = (int) cardType;
        }
    }
}