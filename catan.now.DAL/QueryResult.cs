using System.Collections.Generic;
using System.Data;

namespace Watchtogether.DAL
{
    public class QueryResult
    {
        public List<KeyValuePair<string, object>> Properties { get; } =
            new List<KeyValuePair<string, object>>();

        public QueryResult(IDataRecord reader)
        {
            for (var i = 0; i < reader.FieldCount; i++)
            {
                Properties.Add(reader.GetValue(i).GetType().ToString() != "System.DBNull"
                    ? new KeyValuePair<string, object>(reader.GetName(i), reader.GetValue(i))
                    : new KeyValuePair<string, object>(reader.GetName(i), null));
            }
        }
    }
}