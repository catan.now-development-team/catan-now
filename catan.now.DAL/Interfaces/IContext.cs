﻿using System;
using System.Collections.Generic;
using System.Text;

namespace catan.now.DAL.Interfaces
{
    public interface IContext<T> where T : class
    {
        T Create(T entity);

        T Read(int id);

        T Update(T entity);

        bool Delete(int id);
    }
}
