﻿using System;
using System.Collections.Generic;
using catan.now.Model;

namespace catan.now.DAL.Interfaces
{
    public interface ILayoutContext : IContext<Layout>
    {
        bool Exists(string url);

        Layout GetLayoutInfo(Guid url);
        
        void LinkHexesToLayout(List<Hex> hexes, int id);
        
        IEnumerable<Hex> GetHexesByGuid(Guid guid);
    }
}