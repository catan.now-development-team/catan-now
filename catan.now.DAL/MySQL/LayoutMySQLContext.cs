﻿using System;
using System.Collections.Generic;
using System.Linq;
using catan.now.DAL.Interfaces;
using catan.now.Model;
using Watchtogether.DAL;

namespace catan.now.DAL.MySQL
{
    public class LayoutMySqlContext : MySqlContext<Layout>, ILayoutContext
    {
        public LayoutMySqlContext(string connectionString) : base(connectionString)
        {
        }

        public bool Exists(string url)
        {
            var results = Database.Query("SELECT COUNT(*) FROM layout WHERE token = ?", url);
            return results.Count() != 0;
        }

        public Layout GetLayoutInfo(Guid url)
        {
            var results = Database.Query("SELECT * FROM layout WHERE token = ?", url).ToList();
            if (results.Any())
            {
                return ModelCreator<Layout>.CreateModel(results[0]);
            }

            return null;
        }

        public void LinkHexesToLayout(List<Hex> hexes, int id)
        {
            var query =
                "INSERT INTO hex (layoutid, cardtype, x, porttype, y, rollpoint, stars, northscore, southscore) VALUES ";

            var variables = new List<object>();
            hexes.ForEach(hex =>
            {
                query += "(?, ?, ?, ?, ?, ?, ?, ?, ?)";
                variables.AddRange(new List<object>
                {
                    hex.CardType, hex.X, hex.PortType, hex.Y, hex.RollPoint, hex.Stars,
                    hex.NorthScore, hex.SouthScore
                });

                if (hexes.Last() != hex)
                {
                    query += ",";
                }
            });

            Database.NonQuery(query, variables.ToArray());
        }

        public IEnumerable<Hex> GetHexesByGuid(Guid guid)
        {
            var layout = GetLayoutInfo(guid);

            var results = Database.Query("SELECT * FROM hex WHERE layoutid = ?", layout.Id);

            foreach (var result in results)
            {
                yield return ModelCreator<Hex>.CreateModel(result);
            }
        }
    }
}