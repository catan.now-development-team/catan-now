using System;
using System.Linq;
using System.Reflection;
using catan.now.Model.Utils;
using Watchtogether.DAL;

namespace catan.now.DAL
{
    public static class ModelCreator<T>
    {
        public static T CreateModel(QueryResult properties)
        {
            var model = Activator.CreateInstance<T>();
            var modelProperties = (from propertyInfo in typeof(T).GetProperties()
                from attribute in propertyInfo.GetCustomAttributes()
                where attribute.GetType() == typeof(PropertyAttribute)
                select propertyInfo).ToList();

            foreach (var property in properties.Properties)
            {
                foreach (var modelProperty in modelProperties)
                {
                    if (modelProperty.Name.ToLower().Equals(property.Key.ToLower()))
                    {
                        if (modelProperty.PropertyType == typeof(Guid))
                        {
                            modelProperty.SetValue(model, Guid.Parse(property.Value.ToString()));
                        }
                        else
                        {
                            modelProperty.SetValue(model, property.Value);
                        }

                        break;
                    }
                }
            }

            return model;
        }
    }
}