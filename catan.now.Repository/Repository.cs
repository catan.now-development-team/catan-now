﻿using catan.now.DAL.Interfaces;
using catan.now.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace catan.now.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IContext<T> _context;

        protected Repository(IContext<T> context)
        {
            _context = context;
        }

        public T Create(T entity)
        {
            return _context.Create(entity);
        }

        public T Read(int id)
        {
            return _context.Read(id);
        }

        public T Update(T entity)
        {
            return _context.Update(entity);
        }

        public bool Delete(int id)
        {
            return _context.Delete(id);
        }
    }
}
