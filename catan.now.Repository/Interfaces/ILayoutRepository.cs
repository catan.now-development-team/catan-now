﻿using System;
using System.Collections.Generic;
using System.Text;
using catan.now.Model;
using Models;

namespace catan.now.Repository.Interfaces
{
    public interface ILayoutRepository : IRepository<Layout>
    {
        bool Exists(string url);

        Layout GetLayoutInfo(Guid id);
        
        void LinkHexesToLayout(List<Hex> hexes, int id);
        
        IEnumerable<Hex> GetHexesByGuid(Guid guid);
    }
}