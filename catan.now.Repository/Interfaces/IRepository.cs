﻿namespace catan.now.Repository.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T Create(T entity);

        T Read(int id);

        T Update(T entity);

        bool Delete(int id);
    }
}