﻿using System;
using System.Collections.Generic;
using catan.now.DAL.Interfaces;
using catan.now.Model;
using catan.now.Repository.Interfaces;

namespace catan.now.Repository
{
    public class LayoutRepository : Repository<Layout>, ILayoutRepository
    {
        private readonly ILayoutContext _context;

        public LayoutRepository(ILayoutContext context) : base(context)
        {
            _context = context;
        }

        public bool Exists(string url)
        {
            return _context.Exists(url);
        }

        public Layout GetLayoutInfo(Guid id)
        {
            return _context.GetLayoutInfo(id);
        }

        public void LinkHexesToLayout(List<Hex> hexes, int id)
        {
            _context.LinkHexesToLayout(hexes, id);
        }

        public IEnumerable<Hex> GetHexesByGuid(Guid guid)
        {
            return _context.GetHexesByGuid(guid);
        }
    }
}