using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using catan.now.DTO;
using catan.now.Logic.Interfaces;
using catan.now.Model;
using catan.now.Model.Enums;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Models;

namespace catan.now.Controllers
{
    public class GenerationController : Controller
    {
        private readonly ILayoutLogic _layoutLogic;
        private readonly JsonSerializerSettings _jsonSerializerSettings;

        public GenerationController(Factory.Factory factory)
        {
            _layoutLogic = factory.GetLayoutLogic(Engine.MySql);
            _jsonSerializerSettings = new JsonSerializerSettings
                {Formatting = Formatting.Indented, ContractResolver = new CamelCasePropertyNamesContractResolver()};
        }

        [HttpGet("api/generate")]
        public IActionResult Generate(BoardGenerateDTO dto)
        {
            if (!ModelState.IsValid)
            {
                var modelErrors = new List<KeyValuePair<string, List<string>>>();
                foreach (var (key, value) in ModelState)
                {
                    var errors = new List<string>();
                    foreach (var error in value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }

                    if (errors.Count > 0)
                    {
                        modelErrors.Add(new KeyValuePair<string, List<string>>(key, errors));
                    }
                }

                return Json(new {success = false, errors = modelErrors}, _jsonSerializerSettings);
            }

            var board = new BoardManager(dto.Width, dto.Height, dto.Random, dto.Woolmode);

            return Json(new {success = true, board_token = board.Token, grid = board.Grid}, _jsonSerializerSettings);
        }


        [HttpPost("api/save")]
        public IActionResult Save(string boardToken)
        {
            var layout = new Layout();
            var guid = Guid.Parse(boardToken);
            layout.Token = guid;
            var board = BoardManager.Boards[guid];
            
            layout = _layoutLogic.Create(layout);

            var hexes = new List<Hex>();

            foreach (var hexColumn in board.Grid)
            {
                hexes.AddRange(hexColumn);
            }

            _layoutLogic.LinkHexesToLayout(hexes, layout.Id);

            return Json(new {token = boardToken}, _jsonSerializerSettings);
        }

        [HttpGet("api/layouts")]
        public IActionResult Layouts()
        {
            return null;
        }

        [HttpGet("api/layouts/{token}")]
        public IActionResult Layout(string token)
        {
            Guid guid = Guid.Parse(token);

            var layout = _layoutLogic.GetLayoutInfo(guid);


            if (layout == null)
            {
                return Json(new {success = false, message = "Layout not found"});
            }

            var grid = new List<List<Hex>>();

            foreach (var hex in _layoutLogic.GetHexesByGuid(guid))
            {
                if (grid.Count <= hex.X)
                {
                    grid.Add(new List<Hex>());
                }

                grid[hex.X].Add(hex);
            }

            var returnBoard = new BoardManager(grid, layout.Token);

            return Json(new {success = true, grid = returnBoard.Grid}, _jsonSerializerSettings);
        }
    }
}