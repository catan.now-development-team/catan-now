using System;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using catan.now.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace catan.now.Controllers
{
    public class HomeController : Controller
    {
        [Route("/")]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Dutch()
        {
            //Set cookie for dutch language
            var value = CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(new CultureInfo("nl-NL"), new CultureInfo("nl-NL")));
            var options = new CookieOptions { Expires = DateTime.Now.AddDays(1)};
            Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName, value, options);
            
            //Redirect back to index with new language
            return RedirectToAction("Index", "Home");
        }

        public IActionResult English()
        {
            //Set cookie for english language
            var value = CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(new CultureInfo("en-US"), new CultureInfo("en-US")));
            var options = new CookieOptions { Expires = DateTime.Now.AddDays(1)};
            Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName, value, options);
            
            //Redirect back to index with new language
            return RedirectToAction("Index", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
