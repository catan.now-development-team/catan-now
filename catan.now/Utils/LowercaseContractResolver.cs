using Newtonsoft.Json.Serialization;

namespace catan.now.Utils
{
    public class LowercaseContractResolver: DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }
}