using System.ComponentModel.DataAnnotations;

namespace catan.now.DTO
{
    public class BoardGenerateDTO
    {
        [Required]
        public int? Width { get; set; }

        [Required]
        public int? Height { get; set; }

        [Required]
        public bool? Random { get; set; }
        
        [Required]
        public bool? Woolmode { get; set; }
    }
}