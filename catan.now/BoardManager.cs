using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using catan.now.Model;
using catan.now.Model.Enums;
using Microsoft.EntityFrameworkCore.Internal;
using Models;

namespace catan.now
{
    public class BoardManager
    {
        public static readonly Dictionary<Guid, BoardManager> Boards = new Dictionary<Guid, BoardManager>();

        public List<List<Hex>> Grid { get; private set; }

        private List<Hex> NorthPoles { get; }

        private List<Hex> SouthPoles { get; }

        private List<CardType> AvailableMaterials { get; set; }

        private int Height { get; }

        private int Width { get; }

        public Guid Token { get; }

        private readonly bool _isRandom;
        private readonly bool _isWoolMode;
        private int _desertAmount;


        public BoardManager(int? width, int? height, bool? isRandom = false, bool? isWoolMode = false)
        {
                Token = Guid.NewGuid();
            NorthPoles = new List<Hex>();
            SouthPoles = new List<Hex>();

            if ((int) height > (int) width)
            {
                Height = (int) height;
                Width = (int) width;
            }
            else
            {
                Height = (int) width;
                Width = (int) height;
            }

            _isRandom = (bool) isRandom;
            _isWoolMode = (bool) isWoolMode;


            GenerateGrid();
            GenerateMaterials();
            AddMaterials();
            AddPoints(SortHexes(), GeneratePoints());
            AddScores();
            AssignPorts(GeneratePorts());

            AssignNorthAndSouthPoles();

            Boards.Add(Token, this);
        }


        public BoardManager(List<List<Hex>> grid, Guid token)
        {
            Token = token;
            Grid = grid;
            AssignPorts(GeneratePorts());
        }

        private void GenerateGrid() //Generates a list with the required amount of hexes to fill the size of the grid.
        {
            var value = new List<List<Hex>>();
            for (var w = 0; w < Height; w++)
            {
                var column = new List<Hex>();
                var mid = Width / 2;

                var difference = mid - w;
                if (difference < 0)
                {
                    difference *= -1;
                }

                for (var h = 0; h < Width - difference; h++)
                {
                    column.Add(new Hex(0, 0, CardType.Empty));
                }

                value.Add(column);
            }

            Grid = value;
        }

        private void GenerateMaterials() //Generates a list of the required amount of materials.
        {
            var availableTiles = 0;
            foreach (var column in Grid)
            {
                foreach (var row in column)
                {
                    if (column == Grid.First() || column == Grid.Last() || row == column.First() ||
                        row == column.Last())
                    {
                    }
                    else
                    {
                        availableTiles++;
                    }
                }
            }

            var availableMaterials = new List<CardType>();
            double woodAmount = 1;
            double brickAmount = 0;
            double oreAmount = 0;
            double woolAmount = 1;
            double grainAmount = 1;
            if (availableTiles % 2 == 1)
            {
                for (int i = 0; i < (availableTiles - (availableTiles % 6)) / 6; i++)
                {
                    woodAmount++;
                    brickAmount++;
                    oreAmount++;
                    woolAmount++;
                    grainAmount++;
                }
            }
            else
            {
                for (var i = 1; i < (availableTiles - (availableTiles % 5)) / 5; i++)
                {
                    woodAmount++;
                    brickAmount++;
                    oreAmount++;
                    woolAmount++;
                    grainAmount++;
                }
            }

            for (var i = 0; i < woodAmount; i++)
            {
                availableMaterials.Add(CardType.Wood);
            }

            for (var i = 0; i < brickAmount; i++)
            {
                availableMaterials.Add(CardType.Brick);
            }

            for (var i = 0; i < oreAmount; i++)
            {
                availableMaterials.Add(CardType.Ore);
            }

            for (var i = 0; i < woolAmount; i++)
            {
                availableMaterials.Add(CardType.Wool);
            }

            for (var i = 0; i < grainAmount; i++)
            {
                availableMaterials.Add(CardType.Grain);
            }

            while (availableMaterials.Count < availableTiles)
            {
                _desertAmount++;
                availableMaterials.Add(CardType.Desert);
            }

            AvailableMaterials = availableMaterials;
        }

        private List<int> GeneratePoints() //Generates a list of the required amount of points.
        {
            var availablePoints = new List<int>();
            bool even = GetAllHexesCount() % 6 == 4;
            var requiredPoints = AvailableMaterials.Count;
            ;

            if (_isRandom)
            {
                availablePoints.Add(12);
                availablePoints.Add(2);

                for (int i = 0; i < _desertAmount; i++)
                {
                    requiredPoints--;
                }

                var x = 3;
                for (var i = 2; i < requiredPoints; i++)
                {
                    if (x == 7)
                    {
                        x++;
                    }
                    else if (x == 12)
                    {
                        x = 3;
                    }

                    availablePoints.Add(x);
                    x++;
                }
            }
            else
            {
                if (even)
                {
                    availablePoints.AddRange(new[]
                    {
                        2, 5, 4, 6, 3, 9, 8, 11, 11, 10, 6, 3, 8, 4, 8, 10, 11, 12, 10, 5, 4, 9, 5, 9, 12, 3, 12, 6
                    });
                }
                else
                {
                    availablePoints.AddRange(new[]
                    {
                        5, 2, 6, 3, 8, 10, 9, 12, 11, 4, 8, 10, 9, 4, 5, 6, 3, 11
                    });
                }

                for (int i = 0; i < _desertAmount; i++)
                {
                    requiredPoints--;
                }

                var number = 0;
                while (availablePoints.Count < requiredPoints)
                {
                    number++;
                    availablePoints.Add(availablePoints[number]);
                }
            }

            return availablePoints;
        }

        private List<PortType> GeneratePorts() //generates a list of the required amount of ports.
        {
            var availablePorts = new List<PortType>();
            var portAmount = GetAllPorts(Grid).Count;

            var generalAmount = (4.0 / 9.0) * portAmount;
            var brickAmount = (1.0 / 9.0) * portAmount;
            var oreAmount = (1.0 / 9.0) * portAmount;
            var woolAmount = (1.0 / 9.0) * portAmount;
            var grainAmount = (1.0 / 9.0) * portAmount;
            var woodAmount = (1.0 / 9.0) * portAmount;

            if (_isWoolMode)
            {
                oreAmount -= 1;
                woolAmount += 1;
            }

            for (var i = 0; i < woodAmount; i++)
            {
                availablePorts.Add(PortType.Wood);
            }

            for (var i = 0; i < brickAmount; i++)
            {
                availablePorts.Add(PortType.Brick);
            }

            for (var i = 0; i < oreAmount; i++)
            {
                availablePorts.Add(PortType.Ore);
            }

            for (var i = 0; i < woolAmount; i++)
            {
                availablePorts.Add(PortType.Wool);
            }

            for (var i = 0; i < grainAmount; i++)
            {
                availablePorts.Add(PortType.Grain);
            }

            for (var i = 0; i < generalAmount; i++)
            {
                availablePorts.Add(PortType.General);
            }

            return availablePorts;
        }

        //Loop through every single spot in the map and assign a new hex with a random material
        public void AddMaterials()
        {
            var firstColumn = Grid.First();
            var lastColumn = Grid.Last();
            var availableMaterials = AvailableMaterials.ToList();

            foreach (var column in Grid)
            {
                for (var i = 0; i < column.Count; i++)
                {
                    if (column == firstColumn || column == lastColumn || i == 0 || i == column.Count - 1)
                    {
                        if (GetNeighbours(Grid, Grid.IndexOf(column), i)
                            .Any(hex => (CardType) hex.CardType == CardType.Water))
                        {
                            column[i] = new Hex(Grid.IndexOf(column), i, CardType.Port);
                        }
                        else
                        {
                            column[i] = new Hex(Grid.IndexOf(column), i, CardType.Water);
                        }
                    }
                    else
                    {
                        availableMaterials = availableMaterials.OrderBy(a => Guid.NewGuid()).ToList();

                        var material = availableMaterials.First();

                        availableMaterials.Remove(material);

                        column[i] = new Hex(Grid.IndexOf(column), i, material);
                    }
                }
            }
        }

        private int GetTotalLayers()
        {
            return (int) Math.Floor(Height / 2.0);
        }

        private Point GetOffsetFromSide(int x)
        {
            var center = Math.Floor((decimal) Height / 2);
            if (x < center)
            {
                return new Point(0, 2);
            }
            else if (x > center)
            {
                return new Point(2, 0);
            }
            else
            {
                return new Point(0, 0);
            }
        }

        public List<Hex> GetAllHexes()
        {
            var hexes = new List<Hex>();
            foreach (var columns in Grid)
            {
                foreach (var hex in columns)
                {
                    if ((CardType) hex.CardType != CardType.Water)
                    {
                        hexes.Add(hex);
                    }
                }
            }

            return hexes;
        }

        public List<Hex> GetAllPorts(List<List<Hex>> hexes)
        {
            var value = new List<Hex>();
            foreach (var columns in hexes)
            {
                foreach (var hex in columns)
                {
                    if ((CardType) hex.CardType == CardType.Port)
                    {
                        value.Add(hex);
                    }
                }
            }

            return value;
        }

        public int GetAllHexesCount()
        {
            var height = (int) Math.Floor(Height / 2.0);
            var width = Width;
            var count = width;
            for (var i = 0; i < height; i++)
            {
                width -= 1;
                count += width * 2;
            }

            return count;
        }

        private List<Hex> GetNeighbours(List<List<Hex>> hexes, int x, int y)
        {
            var neighbours = new List<Hex>();
            var offsets = GetOffsetFromSide(x);

            //x - 1
            if (x != 0 && y < hexes[x - 1].Count)
            {
                neighbours.Add(hexes[x - 1][y]);
            }

            //x - 1  y - 1/y + 1
            if (x != 0 && y > 0)
            {
                neighbours.Add(hexes[x - 1][y - 1 + offsets.X]);
            }


            //x + 1
            if (x != hexes.Count - 1 && y < hexes[x + 1].Count)
            {
                neighbours.Add(hexes[x + 1][y]);
            }

            //x + 1  y - 1/ y + 1
            if (x != hexes.Count - 1 && y != 0)
            {
                neighbours.Add(hexes[x + 1][y - 1 + offsets.Y]);
            }

            //y + 1
            if (y != hexes[x].Count - 1)
            {
                neighbours.Add(hexes[x][y + 1]);
            }

            //y - 1
            if (y != 0)
            {
                neighbours.Add(hexes[x][y - 1]);
            }

            return neighbours;
        }

        private List<List<Hex>> SortHexes()
        {
            var even = false;
            var value = new List<List<Hex>>();
            decimal one = Grid.Count / 2;
            var intone = Convert.ToInt32(Math.Floor(one));
            decimal two = Grid[intone].Count / 2;
            var inttwo = Convert.ToInt32(Math.Floor(two));
            var current = Grid[intone][inttwo];

            if (GetAllHexesCount() % 6 == 4)
            {
                even = true;
            }
            else
            {
                value.Add(new List<Hex>() {current});
            }

            var currentlayer = 0;

            while ((CardType) current.CardType != CardType.Water && (CardType) current.CardType != CardType.Port &&
                   (CardType) current.CardType != CardType.Empty)
            {
                currentlayer++;
                var layer = new List<Hex>();
                current = (Grid[current.X][current.Y - 1]);
                var count = 0;
                while (count < 6 && (CardType) current.CardType != CardType.Water &&
                       (CardType) current.CardType != CardType.Port &&
                       (CardType) current.CardType != CardType.Empty)
                {
                    if (!even)
                    {
                        for (var i = 0; i < currentlayer; i++)
                        {
                            layer.Add(current);
                            switch (count)
                            {
                                case 0:
                                    current = Grid[current.X - 1][current.Y];
                                    break;
                                case 1:
                                    current = Grid[current.X][current.Y + 1];
                                    break;
                                case 2:
                                    current = Grid[current.X + 1][current.Y + 1];
                                    break;
                                case 3:
                                    current = Grid[current.X + 1][current.Y - 1];
                                    break;
                                case 4:
                                    current = Grid[current.X][current.Y - 1];
                                    break;
                                case 5:
                                    current = Grid[current.X - 1][current.Y];
                                    break;
                            }
                        }
                    }
                    else
                    {
                        var fakelayer = currentlayer;
                        if (count == 1 || count == 4)
                        {
                            fakelayer = currentlayer - 1;
                        }
                        else
                        {
                            fakelayer = currentlayer;
                        }

                        for (var i = 0; i < fakelayer; i++)
                        {
                            layer.Add(current);
                            switch (count)
                            {
                                case 0:
                                    current = Grid[current.X - 1][current.Y];
                                    break;
                                case 1:
                                    current = Grid[current.X][current.Y + 1];
                                    break;
                                case 2:
                                    current = Grid[current.X + 1][current.Y + 1];
                                    break;
                                case 3:
                                    current = Grid[current.X + 1][current.Y - 1];
                                    break;
                                case 4:
                                    current = Grid[current.X][current.Y - 1];
                                    break;
                                case 5:
                                    current = Grid[current.X - 1][current.Y];
                                    break;
                            }
                        }
                    }

                    count++;
                }

                count = 0;
                if (layer.Count > 1 && even || layer.Count > 0 && !even)
                {
                    value.Add(layer);
                }
            }

            return value;
        }

        //Loop through every single spot in the map and assign a random rollpoint to the hex in the spot
        private void AddPoints(List<List<Hex>> reversedhexes, List<int> availablePoints)
        {
            var rnd = new Random();
            var indexcount = 0;
            if (_isRandom)
            {
                var points = availablePoints.OrderBy(a => Guid.NewGuid()).ToList();
                availablePoints.Clear();
                foreach (var fiche in points.ToList())
                {
                    if (fiche == 6 || fiche == 8)
                    {
                        availablePoints.Add(fiche);
                        points.Remove(fiche);
                    }
                }

                availablePoints.AddRange(points);
                points.Clear();
                points = availablePoints.ToList();
                availablePoints.Clear();
                for (var i = 0; i < points.Count; i++)
                {
                    availablePoints.Add(points[i]);
                    if (points[i] == 6 || points[i] == 8)
                    {
                        for (var j = 0; j < rnd.Next(2, 4); j++)
                        {
                            availablePoints.Add(points.Last());
                            points.Remove(points.Last());
                        }
                    }
                }
            }

            foreach (var list in reversedhexes)
            {
                if (GetAllHexesCount() % 6 == 4)
                {
                    for (var i = 0; i < 6 - indexcount; i++)
                    {
                        if (list.Count > 0)
                        {
                            var hex = list[0];
                            list.Remove(hex);
                            list.Add(hex);
                        }
                    }

                    indexcount += 2;
                }
                else
                {
                    for (var i = 0; i < 3 - indexcount; i++)
                    {
                        if (list.Count > 0)
                        {
                            var hex = list[0];
                            list.Remove(hex);
                            list.Add(hex);
                        }
                    }

                    indexcount++;
                }

                list.Reverse();
                foreach (var hex in list)
                {
                    // Attach points
                    int point;

                    if ((CardType) hex.CardType == CardType.Water || (CardType) hex.CardType == CardType.Desert ||
                        (CardType) hex.CardType == CardType.Port)
                    {
                        point = 0;
                    }
                    else
                    {
                        point = availablePoints.First();

                        if (point == 8 || point == 6)
                        {
                            var shouldRegen = false;
                            foreach (var h in GetNeighbours(Grid, hex.X, hex.Y))
                            {
                                if (h.RollPoint == 8 || h.RollPoint == 6)
                                {
                                    shouldRegen = true;
                                    break;
                                }
                            }

                            var index = 0;
                            while (shouldRegen)
                            {
                                point = availablePoints[index];

                                if (point != 8 && point != 6)
                                {
                                    shouldRegen = false;
                                }
                                else
                                {
                                    index++;
                                    shouldRegen = true;
                                    if (availablePoints.Count < 2)
                                    {
                                        shouldRegen = false;
                                    }
                                }
                            }
                        }

                        hex.RollPoint = point;
                        availablePoints.Remove(point);
                        AssignStar(hex);
                    }
                }
            }
        }

        public void AssignNorthAndSouthPoles()
        {
            foreach (var column in Grid)
            {
                NorthPoles.AddRange(column);
                SouthPoles.AddRange(column);
            }
        }

        public List<Hex> SetTop3()
        {
            List<Hex> poles = SouthPoles;
            poles.AddRange(NorthPoles);
            List<Hex> value = new List<Hex>();
            foreach (var hex in poles)
            {
                if (value.Count >= 3)
                {
                    foreach (var tophex in value.ToList())
                    {
                        if (hex.NorthScore > tophex.NorthScore && hex.NorthScore != -1)
                        {
                            tophex.NorthBool = false;
                            value.Remove(tophex);
                            value.Add(hex);
                            hex.NorthBool = true;
                        }

                        if (hex.SouthScore > tophex.SouthScore && hex.SouthScore != -1)
                        {
                            tophex.SouthBool = false;
                            value.Remove(tophex);
                            value.Add(hex);
                            hex.SouthBool = true;
                            break;
                        }
                    }
                }
                else
                {
                    value.Add(hex);
                }
            }

            return value;
        }

        public void AssignStar(Hex hex) //Calculate star amount for a hex and assign it to the object.
        {
            if ((CardType) hex.CardType == CardType.Desert || (CardType) hex.CardType == CardType.Water)
            {
                return;
            }
            else if (hex.RollPoint < 7)
            {
                hex.Stars = hex.RollPoint - 1;
            }
            else if (hex.RollPoint > 7)
            {
                hex.Stars = 5 - (hex.RollPoint - 8);
            }
        }

        private List<Hex> GetNeighbourScores(int x, int y) //Get all neighbours of a hex.
        {
            var neighbours = new List<Hex>();
            var offsets = GetOffsetFromSide(x);

            if (y != 0) //If neighbour exists add it to the list.
            {
                neighbours.Add(Grid[x][y - 1]);
            }
            else //If not, create an empty neightbour (DEEP SEA TILES BIATCHHHHH).
            {
                neighbours.Add(new Hex(0, 0, CardType.Empty));
            }

            if (x != 0 && y > 0)
            {
                neighbours.Add(Grid[x - 1][y - 1 + offsets.X]);
            }
            else
            {
                neighbours.Add(new Hex(0, 0, CardType.Empty));
            }

            if (x != 0 && y < Grid[x - 1].Count)
            {
                neighbours.Add(Grid[x - 1][y]);
            }
            else
            {
                neighbours.Add(new Hex(0, 0, CardType.Empty));
            }

            if (y != Grid[x].Count - 1)
            {
                neighbours.Add(Grid[x][y + 1]);
            }
            else
            {
                neighbours.Add(new Hex(0, 0, CardType.Empty));
            }

            if (x != Grid.Count - 1 && y < Grid[x + 1].Count)
            {
                neighbours.Add(Grid[x + 1][y]);
            }
            else
            {
                neighbours.Add(new Hex(0, 0, CardType.Empty));
            }

            if (x != Grid.Count - 1 && y != 0)
            {
                neighbours.Add(Grid[x + 1][y - 1 + offsets.Y]);
            }
            else
            {
                neighbours.Add(new Hex(0, 0, CardType.Empty));
            }

            return neighbours;
        }

        private List<int> GetScores(Hex hex) //calculate 3 point scores of a hex
        {
            var scores = new List<int>();

            var neighbours = GetNeighbourScores(hex.X, hex.Y);
            foreach (var neighbour in neighbours)
            {
                var score = hex.Stars;

                Hex neighbour2;

                if (neighbour == neighbours.Last())
                {
                    neighbour2 = neighbours.First();
                }
                else
                {
                    neighbour2 = neighbours[neighbours.IndexOf(neighbour) + 1];
                }

                score += neighbour.Stars;
                score += neighbour2.Stars;

                scores.Add(score);
            }

            return scores;
        }

        public void AddScores() //Assign 3 point score to hexes.
        {
            foreach (var column in Grid)
            {
                for (var i = 0; i < column.Count; i++)
                {
                    var north = true;
                    var south = true;

                    var midIndex = Grid.Count / 2;
                    var index = Grid.IndexOf(column);
                    var columnBiggerThanMid = index > midIndex;
                    var columnSmallerThanMid = index < midIndex;
                    var columnIsMid = index == midIndex;
                    if (((CardType) column[i].CardType == CardType.Water ||
                         (CardType) column[i].CardType == CardType.Port) &&
                        columnBiggerThanMid)
                    {
                        south = false;
                    }

                    if (((CardType) column[i].CardType == CardType.Water ||
                         (CardType) column[i].CardType == CardType.Port) &&
                        columnSmallerThanMid)
                    {
                        north = false;
                    }

                    if (((CardType) column[i].CardType == CardType.Water ||
                         (CardType) column[i].CardType == CardType.Port) && columnIsMid)
                    {
                        south = false;
                        north = false;
                    }

                    var scores = GetScores(column[i]);
                    column[i].NorthScore = north ? scores[1] : -1;
                    column[i].SouthScore = south ? scores[4] : -1;
                }
            }
        }


        private void AssignPorts(List<PortType> availablePorts) //Assign port type to all ports.
        {
            var rnd = new Random();
            foreach (var column in Grid)
            {
                foreach (var hex in column)
                {
                    if ((CardType) hex.CardType == CardType.Port)
                    {
                        var portCount = availablePorts.Count;
                        var random = rnd.Next(1, portCount);

                        var portIndex = random - 1;

                        var selectedPort = availablePorts[portIndex];

                        availablePorts.RemoveAt(portIndex);

                        hex.PortType = (int) selectedPort;
                    }
                }
            }
        }
    }
}