﻿// ’secret’ specifies the numerical keystrokes that make up the word “mario”
var secret = "38384040373937396665";
var input = "";
var timer;

// The following function sets a timer that checks for user input.
$(document).keyup(function (e) {
    input += e.which;
    clearTimeout(timer);
    timer = setTimeout(function () { input = ""; }, 1500);
    check_input();

    // hide answer + error message
    $('.alert').hide();
    $('.ridde-solved').hide();
});

// Once the time is up, this function is run to see if the user’s input is the same as the secret code
function check_input() {
    if (input == secret) {
        $("#easteregg-barbarossa-dialog").modal()
    }
};

function checkAnswer() {
    var language = getCookie("language");
    var answer = $('.riddle-answer').val().toLowerCase();

    if (language == "dutch") {
        // dutch
        if (answer == "lepel") {
            $('.riddle').fadeOut(200);
            $('.riddle-solved').delay(200).fadeIn(200);
        }
        else {
            $('.alert').fadeIn(100);
        }
    }
    else {
        // english

        if (answer == "rainbow") {
            $('.riddle').fadeOut(200);
            $('.riddle-solved').delay(200).fadeIn(200);
        }
        else {
            $('.alert').fadeIn(100);
        }
    }
}