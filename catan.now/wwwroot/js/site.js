﻿function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

$('.copy-code').click(function () {
    const copyText = $(".generate-code");
    copyText.select();
    document.execCommand("copy");
    $(this).text("Copied");
});

function changeTheme() {
    $('body').toggleClass('dark');
    $('.sidebar').toggleClass('darkSidebar');
    $(".btn-sidebar").toggleClass('darkButton');
    $(".btn-option").toggleClass('darkButton');
    $(".btn-download").toggleClass('darkButton');
    $(".btn-load").toggleClass('darkButton');
    $(".toggler").toggleClass('darkToggle fa-sun fa-moon');
    $("#languageTitle").toggleClass('darkLanguage');
    $("#cb1").toggleClass('tgl-dark');
    $("#cb2").toggleClass('tgl-dark');
}

function changeLanguage(lang) {
    $(".imgFlag").attr("src", "/images/" + lang + ".svg");
}

function getBoardArray(board) {
    let hexes = [];

    board.columns.forEach(function (column, index) {
        column.hexagons.forEach(function (hexagon, index2) {
            hexes.push({
                x: hexagon.x,
                y: hexagon.y,
                portType: hexagon.portType,
                cardType: hexagon.cardType,
                rollPoint: hexagon.number,
                stars: hexagon.stars,
                northScore: hexagon.northScore,
                southScore: hexagon.southScore,
            });
        });
    });

    return hexes;
}

function GetIsRandom() {
    if (document.getElementById('random_true').checked) {
        return true;
    } else {
        return false;
    }
}

function GetIsWoolMode() {
    if (document.getElementById('woolmode').checked) {
        return true;
    } else {
        return false;
    }
}

function GetSelectedSizeX() {
    if (document.getElementById('mapsize_big').checked) {
        return 8;
    } else if (document.getElementById('mapsize_normal').checked) {
        return 7;
    }
}

function GetSelectedSizeY() {
    if (document.getElementById('mapsize_big').checked) {
        return 9;
    } else if (document.getElementById('mapsize_normal').checked) {
        return 7;
    }
}

$(document).ready(function () {
    $('.collapse.in').prev('.panel-heading').addClass('active');
    $('#accordion, #bs-collapse')
        .on('show.bs.collapse', function (a) {
            $(a.target).prev('.panel-heading').addClass('active');
        })
        .on('hide.bs.collapse', function (a) {
            $(a.target).prev('.panel-heading').removeClass('active');
        });

    var theme = getCookie("theme");
    var language = getCookie("language");

    if (theme == "dark") {
        changeTheme();
    }

    changeLanguage(language);

    $('#cb1').prop('checked', true);
    $('#cb2').prop('checked', false);

    // Toggle dark theme
    $('.theme-toggle').click(function () {
        changeTheme();

        if (theme === "dark") {
            theme = "light";
        } else {
            theme = "dark";
        }

        document.cookie = "theme=" + theme;
    });

    $.get("/api/layouts").then(({layouts}) => {
        layouts.forEach(function (layout) {
            let layoutElement = document.createElement("div");
            layoutElement.classList.add("layout");

            let layoutNameElement = document.createElement("div");
            layoutNameElement.classList.add("layoutname");
            layoutNameElement.innerText = btoa(layout.id);

            let layoutButtonElement = document.createElement("button");
            layoutButtonElement.classList.add("btn");
            layoutButtonElement.classList.add("btn-success");
            layoutButtonElement.classList.add("loadlayout");
            layoutButtonElement.setAttribute("data-layout", btoa(layout.id));
            layoutButtonElement.innerText = "Load";

            layoutElement.append(layoutNameElement);
            layoutElement.append(layoutButtonElement);

            $(".layoutlist").append(layoutElement);
        })
    });

    $("#load-map-dialog").on("click", ".loadlayout", function (e) {
        let layout = $(e.target).data("layout");
        generateHexagonsFromDatabase(layout);
        $("#load-map-dialog").modal("hide");
    });

    // Language button pressed
    $('.languageSwitch').on('click', function () {
        if (language === "dutch") {
            language = "english";
        } else {
            language = "dutch";
        }

        document.cookie = "language=" + language;
        window.location.href = '/home/' + language;
    });

    $('#cb1').change(function () {
        board.setFiches($(this)[0].checked);
    });

    $('#cb2').change(function () {
        board.setThreePoints($(this)[0].checked);
    });

    $("#btndownload").click(function () {
        $.post("api/save", {
                boardToken: boardToken
            }, {
                dataType: "json",
                traditional: true
            }
        ).then(data => {
            $('.generate-code').val(boardToken);
            $('.copy-code').text("Copy");
        });
    });


    $('#copy_code').on('click', function () {
        var copyText = document.getElementById("generatecode");
        copyText.select();
        document.execCommand("copy");
    });

    $('#btnsidebar').on('click', function () {
        generateHexagons(function () {
            if ($('#cb1').is(":checked")) {
                board.setFiches(true);
            } else {
                board.setFiches(false);
            }

            if ($('#cb2').is(":checked")) {
                board.setThreePoints(true);
            }
        });
    });

    $('.close-easteregg').click(function () {
        $('.riddle-solved').hide();
        $('.riddle').show();
    });

});