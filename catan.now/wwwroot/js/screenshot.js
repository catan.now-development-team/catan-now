$(function () {
    $("#btnSave").click(function () {
        html2canvas(document.getElementById("game-canvas")).then(function (canvas) {
            let download = document.createElement('a');
            download.href = canvas.toDataURL();
            download.download = 'catan_layout.png';
            download.click();
        });
    })
});