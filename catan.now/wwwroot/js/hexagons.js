"use strict";

class Hexagon {
    constructor(x, y, cardType, number, stars, northScore, southScore, portType) {
        this.x = x;
        this.y = y;
        this.cardType = cardType;
        this.stars = stars;
        this.number = number;
        this.northScore = northScore;
        this.southScore = southScore;
        this.portType = portType;
    }

    link(element) {
        this.element = element;

        switch (this.cardType) {
            case 0:
                element.classList.add("water");
                break;
            case 1:
                //port
                element.classList.add("water");
                break;
            case 2:
                element.classList.add("wool");
                break;
            case 3:
                element.classList.add("ore");
                break;
            case 4:
                element.classList.add("wood");
                break;
            case 5:
                element.classList.add("brick");
                break;
            case 6:
                element.classList.add("grain");
                break;
            case 7:
                element.classList.add("desert");
                break;
        }
    }


}

class GridColumn {
    constructor(hexagons) {
        this.hexagons = hexagons;
    }
}

class Board {
    constructor(boardData, element) {
        element.empty();
        this.columns = [];
        this.element = element;

        boardData.columns.forEach((columns, index) => this.columns[index] = new GridColumn(columns));
    }

    linkElementsToData() {
        let hexscores = [];
        this.columns.forEach((column) => {
            column.hexagons.forEach(hexagon => {
                if (hexagon.northScore !== -1 && hexagon.southScore !== -1) {
                    hexscores.push(hexagon.southScore);
                    hexscores.push(hexagon.northScore);
                }
            });
        });

        let top3 = [];

        let first = Math.max.apply(Math, hexscores.map(function (o) {
            return o;
        }));
        hexscores.splice(hexscores.indexOf(first), 1);
        top3.push(first);

        let second = Math.max.apply(Math, hexscores.map(function (o) {
            return o;
        }));
        hexscores.splice(hexscores.indexOf(second), 1);
        top3.push(second);

        let third = Math.max.apply(Math, hexscores.map(function (o) {
            return o;
        }));
        hexscores.splice(hexscores.indexOf(first), 1);
        top3.push(third);

        this.columns.forEach((value) => {
            let row = document.createElement("div");
            row.classList.add("hex-row");
            this.element.append(row);

            value.hexagons.forEach(value => {

                let hex = document.createElement("div");
                hex.classList.add("hex");

                if (value.northScore !== null && value.southScore !== null) {
                    let top = value.northScore;
                    let south = value.southScore;

                    let northElement = document.createElement("div");
                    northElement.innerText = top;
                    northElement.classList.add("pole", "north-pole");
                    if ($.inArray(top, top3) > -1) {
                        top3.splice(top3.indexOf(top), 1);
                        northElement.classList.add("top3");
                    }

                    let southElement = document.createElement("div");
                    southElement.innerText = south;
                    southElement.classList.add("pole", "south-pole");
                    if ($.inArray(south, top3) > -1) {
                        top3.splice(top3.indexOf(south), 1);
                        southElement.classList.add("top3");
                    }

                    let numberElement = document.createElement("div");
                    numberElement.innerText = value.number;
                    numberElement.classList.add("number");
                    numberElement.style.fontWeight = "bold";
                    if (value.number === 8 || value.number === 6) {
                        numberElement.style.color = "red";

                    }

                    let starsElement = document.createElement("div");
                    for (let i = 0; i < value.stars; i++) {
                        starsElement.innerText += "*";
                    }
                    starsElement.classList.add("stars");

                    let ficheElement = document.createElement("div");
                    ficheElement.appendChild(numberElement);
                    ficheElement.appendChild(starsElement);
                    ficheElement.classList.add("fiche");

                    let backgroundElement = document.createElement("div");
                    backgroundElement.classList.add("background");
                    switch (value.cardType) {
                        case 0:
                            break;
                        case 2:
                            backgroundElement.classList.add("background-wool");
                            break;
                        case 3:
                            backgroundElement.classList.add("background-ore");
                            break;
                        case 4:
                            backgroundElement.classList.add("background-wood");
                            break;
                        case 5:
                            backgroundElement.classList.add("background-brick");
                            break;
                        case 6:
                            backgroundElement.classList.add("background-grain");
                            break;
                        default:
                            break;
                    }

                    if (value.cardType === 1) {
                        let portElement = document.createElement("div");
                        let portBackgroundElement = document.createElement("div");
                        let portDirectionElement = document.createElement("div");

                        portBackgroundElement.classList.add("background");
                        portBackgroundElement.classList.add("port-background");
                        portElement.classList.add("port");
                        portElement.style.fontWeight = "bold";

                        switch (value.portType) {
                            case 0:
                                break;
                            case 1: // 3 : 1 - ?
                                portBackgroundElement.classList.add("background-question");
                                portElement.innerText = "3 : 1";
                                break;
                            case 2: // 2 : 1 - Wool
                                portBackgroundElement.classList.add("background-wool");
                                portElement.innerText = "2 : 1";
                                break;
                            case 3: // 2 : 1 - Wood
                                portBackgroundElement.classList.add("background-wood");
                                portElement.innerText = "2 : 1";
                                break;
                            case 4: // 2 : 1 - Ore
                                portBackgroundElement.classList.add("background-ore");
                                portElement.innerText = "2 : 1";
                                break;
                            case 5: // 2 : 1 - Brick
                                portBackgroundElement.classList.add("background-brick");
                                portElement.innerText = "2 : 1";
                                break;
                            case 6: // 2 : 1 - Grain
                                portBackgroundElement.classList.add("background-grain");
                                portElement.innerText = "2 : 1";
                                break;
                            default:
                                break;
                        }

                        portDirectionElement.classList.add("port-direction-background");
                        // Looks what direction has the most tiles -> if so use the correct picture
                        switch (indexOfMax(this.GetPortSideScore(value))) {
                            case 0: // nw
                                portDirectionElement.classList.add("direction-nw");
                                break;
                            case 1: // ne
                                portDirectionElement.classList.add("direction-ne");
                                break;
                            case 2: // e
                                portDirectionElement.classList.add("direction-e");
                                break;
                            case 3: // se
                                portDirectionElement.classList.add("direction-se");
                                break;
                            case 4: // sw
                                break;
                            case 5: // w
                                portDirectionElement.classList.add("direction-w");
                                break;
                            default:
                                break;
                        }

                        hex.appendChild(portDirectionElement);
                        hex.appendChild(portBackgroundElement);
                        hex.appendChild(portElement);
                    }


                    hex.appendChild(backgroundElement);
                    if (top !== -1) {
                        hex.appendChild(northElement);
                    }
                    if (south !== -1) {
                        hex.appendChild(southElement);
                    }
                    if (value.cardType !== 0 && value.cardType !== 1 && value.cardType !== 7) {
                        hex.appendChild(ficheElement);
                    }

                }

                row.appendChild(hex);
                value.link(hex);


            })
        })
    }

    setFiches(state) {
        if (state) {
            $(".fiche").addClass("visible");
        } else {
            $(".fiche").removeClass("visible");
        }
    }

    setThreePoints(state) {
        if (state) {
            $(".pole").addClass("visible");
        } else {
            $(".pole").removeClass("visible");
        }
    }

    getOffsetFromSide(x) {
        let center = Math.floor(this.columns.length / 2);
        if (x < center) {
            return {
                x: 0,
                y: 2
            }
        } else if (x > center) {
            return {
                x: 2,
                y: 0
            }
        } else {
            return {
                x: 0, y: 0
            }
        }
    }

    GetPortSideScore(hexData) {
        // Get all of the hexes and the half point to calculate  sides
        var allHexes = this.GetAllHexes();
        var halfPoint = this.GetHalfPointMap(allHexes);

        // Put the side points into an array
        var sides = [];
        sides.push(this.GetScoreNW(allHexes, hexData, halfPoint));
        sides.push(this.GetScoreNE(allHexes, hexData, halfPoint));
        sides.push(this.GetScoreE(allHexes, hexData));
        sides.push(this.GetScoreSE(allHexes, hexData, halfPoint));
        sides.push(this.GetScoreSW(allHexes, hexData, halfPoint));
        sides.push(this.GetScoreW(allHexes, hexData));

        // return the sides array.
        return sides;
    }

    GetAllHexes() {
        var allHexes = [];

        //Put all hexes in a list so we can loop through them
        this.columns.forEach((column) => {
            column.hexagons.forEach(hexagon => {
                allHexes.push(hexagon);
            });
        });

        return allHexes;
    }

    GetHalfPointMap(allHexes) {
        var halfPoint = 0;

        //Calculate the half point on the standard map
        for (var i = 0; i < allHexes.length; i++) {
            if (allHexes[i].x > halfPoint) {
                halfPoint = allHexes[i].x;
            }
        }
        // Half of the x value -> will be the center (halfpoint)
        halfPoint = halfPoint / 2;

        return halfPoint;
    }

    GetScoreNW(allHexes, hexData, halfPoint) {
        var score = 0;
        var nextX = 1;
        var nextY = 1;

        for (var i = 0; i < allHexes.length; i++) {

            // If on half point we need to stay on the same Y-coord
            if (allHexes[i].x >= halfPoint && allHexes[i].y == hexData.y - nextY && allHexes[i].x == hexData.x - nextX) {
                // We put nextY back to 0 to get this result
                nextY = 0;
            }

            // If a hex is found with the new coordinates:
            if (allHexes[i].y == hexData.y - nextY && allHexes[i].x == hexData.x - nextX) {
                // Put the score, y & x up.
                score++;
                nextY++;
                nextX++;

                // Start the i at -1 so we loop through all again
                i = -1;
            }
        }

        return score;
    }

    GetScoreNE(allHexes, hexData, halfPoint) {
        var score = 0;
        var nextX = 1;
        var nextY = 1;

        for (var i = 0; i < allHexes.length; i++) {

            // If on half point we need to stay on the same Y-coord
            if (allHexes[i].x >= halfPoint && allHexes[i].y == hexData.y + nextY - 1 && allHexes[i].x == hexData.x - nextX) {
                // We put nextY back to 0 to get this result
                nextY = 0;
            }

            // If a hex is found with the new coordinates:
            if (allHexes[i].y == hexData.y + nextY && allHexes[i].x == hexData.x - nextX) {
                // Put the score, y & x up.
                score++;
                nextY++;
                nextX++;

                // Start the i at -1 so we loop through all again
                i = -1;
            }
        }

        return score;
    }

    GetScoreE(allHexes, hexData) {
        var score = 0;
        var nextY = 1;

        // This one only goes to the right, so we only up the y.
        for (var i = 0; i < allHexes.length; i++) {
            if (allHexes[i].y == hexData.y + nextY && allHexes[i].x == hexData.x) {
                score++;
                nextY++;

                // Start the i at -1 so we loop through all again
                i = -1;
            }
        }

        return score;
    }

    GetScoreSE(allHexes, hexData, halfPoint) {
        var score = 0;
        var nextX = 1;
        var nextY = 1;

        // If on half point we need to go one lower
        for (var i = 0; i < allHexes.length; i++) {
            if (allHexes[i].x > halfPoint && allHexes[i].y == hexData.y + (nextY - 1) && allHexes[i].x == hexData.x + nextX) {
                // We put nextY 1 lower to get this result
                nextY = nextY - 1;
            }

            // If a hex is found with the new coordinates:
            if (allHexes[i].y == hexData.y + nextY && allHexes[i].x == hexData.x + nextX) {
                // Put the score, y & x up.
                score++;
                nextY++;
                nextX++;

                // Start the i at -1 so we loop through all again
                i = -1;
            }
        }

        return score;
    }

    GetScoreSW(allHexes, hexData, halfPoint) {
        var score = 0;
        var nextX = 1;
        var nextY = 1;

        // If on half point we need to stay on the same Y-coord
        for (var i = 0; i < allHexes.length; i++) {
            if (allHexes[i].x <= halfPoint && allHexes[i].y == hexData.y - nextY && allHexes[i].x == hexData.x + nextX) {
                // We put nextY back to 0 to get this result
                nextY = 0;
            }

            // If a hex is found with the new coordinates:
            if (allHexes[i].y == hexData.y - nextY && allHexes[i].x == hexData.x + nextX) {
                // Put the score, y & x up.
                score++;
                nextY++;
                nextX++;

                i = -1;
            }
        }

        // Start the i at -1 so we loop through all again
        return score;
    }

    GetScoreW(allHexes, hexData) {
        var score = 0;
        var nextY = 1;

        // This one only goes to the left, so we only up the y.
        for (var i = 0; i < allHexes.length; i++) {
            if (allHexes[i].y == hexData.y - nextY && allHexes[i].x == hexData.x) {
                score++;
                nextY++;

                // Start the i at -1 so we loop through all again
                i = -1;
            }
        }

        return score;
    }

    getNeighbours(hexData) {
        let neighbours = [];
        let offsets = this.getOffsetFromSide(hexData.x);

        if (this.columns[hexData.x - 1] !== undefined) {
            let column = this.columns[hexData.x - 1];
            if (column.hexagons[hexData.y] !== undefined) {
                neighbours.push(column.hexagons[hexData.y]);
            }

            if (column.hexagons[hexData.y - 1] !== undefined) {
                neighbours.push(column.hexagons[hexData.y - 1 + offsets.x]);
            }
        }

        if (this.columns[hexData.x + 1] !== undefined) {
            let column = this.columns[hexData.x + 1];
            if (column.hexagons[hexData.y] !== undefined) {
                neighbours.push(column.hexagons[hexData.y]);
            }

            if (column.hexagons[hexData.y - 1 + offsets.y] !== undefined) {
                neighbours.push(column.hexagons[hexData.y - 1 + offsets.y]);
            }
        }

        let column = this.columns[hexData.x];
        if (column.hexagons[hexData.y + 1] !== undefined) {
            neighbours.push(column.hexagons[hexData.y + 1]);

        }
        if (column.hexagons[hexData.y - 1] !== undefined) {
            neighbours.push(column.hexagons[hexData.y - 1]);
        }
        return neighbours;
    }

    getDataFromElement(element) {
        for (let y = 0; y < this.columns.length; y++) {
            for (let x = 0; x < this.columns[y].hexagons.length; x++) {
                let hexData = this.columns[y].hexagons[x];
                if (element === hexData.element) {
                    return hexData;
                }
            }
        }
        return null;
    }
}

let board;
let boardToken;

$(function () {
    generateHexagons();
});

function generateHexagonsFromDatabase(hash) {
    loadHexagonsFromDatabase(hash, function (result) {
        board = new Board(result.board, $(".hex-grid"));
        board.linkElementsToData();
        board.setFiches(true);

        $('.pole').click(function (e) {
            $(this).addClass('village');
        });

        if (cb) {
            cb();
        }
    });
}

function generateHexagons(cb) {
    loadHexagons(function (result) {
        board = new Board(result.board, $(".hex-grid"));
        board.linkElementsToData();
        board.setFiches(true);

        $('.pole').click(function (e) {
            $(this).addClass('village');
        });

        if (cb) {
            cb();
        }
    });
}

function loadHexagonsFromDatabase(hash, cb) {
    $(".overlay").addClass("active");
    $.get('api/layouts/' + hash, {
        width: GetSelectedSizeX(),
        height: GetSelectedSizeY(),
        random: GetIsRandom(),
        woolmode: GetIsWoolMode()
    }).then(data => {
        boardToken = data["board_token"];
        console.log(data.grid);
        let result = {
            board: {
                columns: []
            }
        };
        $(".overlay").removeClass("active");
        for (let columnIndex in data.grid) {
            let columns = data.grid[columnIndex];
            result.board.columns[columnIndex] = [];
            for (let hexIndex in columns) {
                let hex = columns[hexIndex];
                let hexagon = new Hexagon(hex.x, hex.y, hex.cardType, hex.rollPoint, hex.stars, hex.northScore, hex.southScore, hex.portType);
                result.board.columns[columnIndex].push(hexagon);
            }
        }

        console.log(result);

        if (cb) {
            cb(result);
        }
    });
}


function loadHexagons(cb) {
    console.log(GetIsWoolMode());
    $(".overlay").addClass("active");
    $.get('api/generate', {
        width: GetSelectedSizeX(),
        height: GetSelectedSizeY(),
        random: GetIsRandom(),
        woolmode: GetIsWoolMode()
    }).then(data => {
        console.log(data.grid);
        boardToken = data["board_token"];
        let result = {
            board: {
                columns: []
            }
        };
        $(".overlay").removeClass("active");
        for (let columnIndex in data.grid) {
            let columns = data.grid[columnIndex];
            result.board.columns[columnIndex] = [];
            for (let hexIndex in columns) {
                let hex = columns[hexIndex];
                let hexagon = new Hexagon(hex.x, hex.y, hex.cardType, hex.rollPoint, hex.stars, hex.northScore, hex.southScore, hex.portType);
                result.board.columns[columnIndex].push(hexagon);
            }
        }

        console.log(result);

        if (cb) {
            cb(result);
        }
    });
}

//Get the index of the highest number in the given array
function indexOfMax(arr) {
    //If array is empty, return -1
    if (arr.length === 0) {
        return -1;
    }

    //Set default
    var max = arr[0];
    var maxIndex = 0;

    //Loop through the array
    for (var i = 1; i < arr.length; i++) {
        //If the next number is higher than the max, then set this number to the new max
        if (arr[i] > max) {
            maxIndex = i;
            max = arr[i];
        }
    }

    //Return index
    return maxIndex;
}
